#include <stdio.h>
#include <stdlib.h>

typedef struct {
	int vymenX;
	int vymenY;
	int porovnajX;
	int porovnajY;
} BOD;

void bubbleSort(int *pole, int velkost, FILE *f, BOD *bod);
void shellSort(int *pole, int velkost, FILE *f, BOD *bod);
void endSvg(FILE *f);
void beginSvg(FILE *f);



int main()
{
	BOD bubble, shell;
	BOD *p_bubble = &bubble;
	BOD *p_shell = &shell;
	
	int *pole1;
	int *pole2;
	
	FILE *f;
	
	if((f = fopen("svg_sausova.svg","w")) == NULL)
		return 1;
	
	beginSvg(f);
	
	for(int i = 5; i <= 70; i++)
	{
		pole1 = malloc(sizeof(int) * i);
		pole2 = malloc(sizeof(int) * i);
		
		for(int j = 0; j < i; j++)
		{
			pole1[j] = rand()%i;
			pole2[j] = pole1[j];
		}
		
		bubbleSort(pole1, i, f, p_bubble);
		shellSort(pole2, i, f, p_shell);
		
		free(pole1);
		free(pole2);
	}
	
	endSvg(f);
	fclose(f);
	
	return 0;
}

void bubbleSort(int *pole, int velkost, FILE *f, BOD *bod)
{
	int vymen = 0;
	int porovnaj = 0;
	int swap;
	
	for (int c = 0 ; c < ( velkost - 1 ); c++)
    {
		for (int d = 0 ; d < (velkost - c - 1); d++)
		{
			if (pole[d] > pole[d+1])
			{
				swap = pole[d];
				pole[d] = pole[d+1];
				pole[d+1] = swap;
				vymen++;
			}
			porovnaj++;
		}
	}
	
	if(velkost == 5)
	{
		bod->porovnajX = velkost * 10;
		bod->porovnajY = 700 - 20 - porovnaj/5;
		bod->vymenX = velkost * 10;
		bod->vymenY = 700 - 20 - vymen/5;
	}
	
	fprintf(f,"<circle cx=\"%d\" cy=\"%d\" r=\"2\"   stroke=\"red\"/>\n", velkost * 10, 700 - 20 - porovnaj/5);
	fprintf(f,"<circle cx=\"%d\" cy=\"%d\" r=\"2\"   stroke=\"blue\"/>\n", velkost * 10, 700 - 20 - vymen/5);
	
	fprintf(f,"<line x1=\"%d\" x2=\"%d\" y1=\"%d\" y2=\"%d\" stroke=\"red\"/>\n",velkost * 10, bod->porovnajX, 700 - 20 - porovnaj/5, bod->porovnajY);
	fprintf(f,"<line x1=\"%d\" x2=\"%d\" y1=\"%d\" y2=\"%d\" stroke=\"blue\"/>\n",velkost * 10, bod->vymenX, 700 - 20 - vymen/5, bod->vymenY);

	bod->porovnajX = velkost * 10;
	bod->porovnajY = 700 - 20 - porovnaj/5;
	bod->vymenX = velkost * 10;
	bod->vymenY = 700 - 20 - vymen/5;
}

void shellSort(int *pole, int velkost, FILE *f, BOD *bod)
{
	int vymen = 0;
	int porovnaj = 0;
	int swap;
	
	for(int i = velkost/2; i > 0; i = i/2)
	{
		for(int j = i; j < velkost; j++)
		{
			for(int k = j - i; k >= 0; k = k - i)
			{
				if(pole[k + i] >= pole[k])
				{
					porovnaj++;
					break;
				}
				else
				{
					vymen++;
					swap = pole[k];
					pole[k] = pole[k+i];
					pole[k+i] = swap;
				}
			}
		}
	}
	
	if(velkost == 5)
	{
		bod->porovnajX = velkost * 10;
		bod->porovnajY = 700 - 20 - porovnaj/5;
		bod->vymenX = velkost * 10;
		bod->vymenY = 700 - 20 - vymen/5;
	}
	
	fprintf(f,"<circle cx=\"%d\" cy=\"%d\" r=\"2\"   stroke=\"black\"/>\n", velkost * 10, 700 - 20 - porovnaj/5);
	fprintf(f,"<circle cx=\"%d\" cy=\"%d\" r=\"2\"   stroke=\"green\"/>\n", velkost * 10, 700 - 20 - vymen/5);
	
	fprintf(f,"<line x1=\"%d\" x2=\"%d\" y1=\"%d\" y2=\"%d\" stroke=\"black\"/>\n",velkost * 10, bod->porovnajX, 700 - 20 - porovnaj/5, bod->porovnajY);
	fprintf(f,"<line x1=\"%d\" x2=\"%d\" y1=\"%d\" y2=\"%d\" stroke=\"green\"/>\n",velkost * 10, bod->vymenX, 700 - 20 - vymen/5, bod->vymenY);

	bod->porovnajX = velkost * 10;
	bod->porovnajY = 700 - 20 - porovnaj/5;
	bod->vymenX = velkost * 10;
	bod->vymenY = 700 - 20 - vymen/5;
}

void beginSvg(FILE *f)
{
	fprintf(f,"<?xml version=\"1.0\"?>\n<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
	fprintf(f,"<svg xmlns=\"http://www.w3.org/2000/svg\" width = \"750\" height = \"700\" version = \"1.1\">\n");
	
	fprintf(f,"<line x1=\"0\" x2=\"730\" y1=\"680\" y2=\"680\" stroke=\"black\"/>\n"); // x os
	fprintf(f,"<line x1=\"20\" x2=\"20\" y1=\"20\" y2=\"700\" stroke=\"black\"/>\n"); // y os
	
	fprintf(f,"<text x=\"%d\" y=\"%d\" fill=\"black\">pocet prvkov na triedenie</text>\n",21,695);
	fprintf(f,"<text x=\"%d\" y=\"%d\" fill=\"red\">Bubble Sort - porovnania</text>\n",21,20);
	fprintf(f,"<text x=\"%d\" y=\"%d\" fill=\"blue\">Bubble Sort - vymeny</text>\n",21,40);
	fprintf(f,"<text x=\"%d\" y=\"%d\" fill=\"black\">Shell Sort - porovnania</text>\n",21,60);
	fprintf(f,"<text x=\"%d\" y=\"%d\" fill=\"green\">Shell Sort - vymeny</text>\n",21,80);
}

void endSvg(FILE *f)
{
	fprintf(f,"\n</svg>");
}
